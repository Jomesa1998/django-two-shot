from django.shortcuts import render
from receipts.models import Receipt


# Create your views here.
def receipt_list(request):
    receipt = Receipt.objects.all()
    context = {"receipt": receipt}
    return render(request, "receipts/receipt_list.html", context)
